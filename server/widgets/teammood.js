const axios = require('axios');
const env = require('../../env.json');

module.exports = (app) => {
  app.post('/api/teammood', (req, res) => {
    axios.get(`https://beta.teammood.com/api/${env.teammood.apiKey}/moods`)
    .then(response => {
      res.json({
        date: response.data.days[0].date,
        moods: response.data.days[0].values.slice(0, 5)
      });
    })
    .catch(error => {
      res.json({
        error: error
      });
    });
  });
};