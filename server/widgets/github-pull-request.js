const axios = require('axios');
const octokit = require('@octokit/rest')();
const env = require('../../env.json');

module.exports = (app) => {
  app.get('/api/github-pull-request', (req, res) => {
    octokit.authenticate(env.github.authentication);

    octokit.pullRequests.getAll({
      owner: req.query.owner,
      repo: req.query.repository,
      state: 'open'
    })
    .then(response => {
      res.json(req.query.author !== '' ? response.data.filter(pr => pr.user.login === req.query.author) : response.data);
    })
    .catch(error => {
      res.json({
        error: error
      });
    });
  });
};