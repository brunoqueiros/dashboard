const axios = require('axios');

module.exports = (app) => {
  app.get('/api/daily-dilbert', (req, res) => {
    axios.get('http://dilbert.com')
    .then(response => {
      const imgTag = response.data.match(/<img[^>]*src=\"http:\/\/assets\.amuniversal\.com\/([a-zA-Z0-9]+)\"[^>]*\/?>/)[0];

      res.json({
        alt: imgTag.match(/alt="([a-zA-Z0-9 ]+)/)[0].split('alt="')[1],
        src: imgTag.match(/src="([a-zA-Z0-9://.]+)/)[0].split('src="')[1]
      });
    })
    .catch(error => {
      res.json({
        error: error
      });
    });
  });
};