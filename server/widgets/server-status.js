const axios = require('axios');

module.exports = (app) => {
  app.get('/api/server-status', (req, res) => {
    axios.get(req.query.site, {
      timeout: 5000
    })
    .then(response => {
      res.send(response.status === 200 ? 'up' : 'down');
    })
    .catch(error => {
      res.send('down');
    });
  });
};