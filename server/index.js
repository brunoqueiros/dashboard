const express = require('express');
const app = express();

function loadWidgets() {
  var normalizedPath = require('path').join(__dirname, 'widgets');

  require('fs').readdirSync(normalizedPath).forEach(function(file) {
    console.log('./widgets/' + file);
    require('./widgets/' + file)(app);
  });
}

loadWidgets();

app.use(function(req, res, next) {
  res.header("Access-Control-Allow-Origin", "*");
  res.header("Access-Control-Allow-Headers", "Origin, X-Requested-With, Content-Type, Accept");
  next();
});

app.all('*', function(req, res, next) {
  res.header('Access-Control-Allow-Origin', '*');
  res.header('Access-Control-Allow-Methods', 'PUT, GET, POST, DELETE, OPTIONS');
  res.header('Access-Control-Allow-Headers', 'Content-Type');
  next();
});

app.get('/', (req, res) => res.send('Hello World!'));

app.listen(3000, () => console.log('Example app listening on port 3000!'));