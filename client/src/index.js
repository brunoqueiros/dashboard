import Vue from 'vue'
import dragula from 'dragula'
import App from './App.vue'

Vue.config.productionTip = false

new Vue({
  render: h => h(App)
}).$mount('#app')

dragula([document.querySelector('.container')])